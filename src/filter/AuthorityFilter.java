package filter;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;


@WebFilter(urlPatterns={"/management","/edit","/signup"})
public class AuthorityFilter implements Filter{


	@Override
	  public void doFilter(ServletRequest request, ServletResponse response,
	      FilterChain chain) throws IOException, ServletException {


		  HttpSession session = ((HttpServletRequest) request).getSession();

		  //ログインフィルターからかける
		  if(session.getAttribute("loginUser") == null){
			  ((HttpServletResponse) response).sendRedirect("LoginFilter");
	            return;
		  }

		  //権限確認
		  User user = (User) session.getAttribute("loginUser");
		  int position_id = (user.getPosition_id());
		  if(position_id == 3){
				  	chain.doFilter(request, response);
		            return;
		  }else{
			  	List<String> messages = new ArrayList<String>();
	            messages.add("権限がありません。");
	            session.setAttribute("errorMessages", messages);
	            ((HttpServletResponse) response).sendRedirect("./");
	            return;
		  }
  }

public void init(FilterConfig filterConfig) throws ServletException{
  }

  public void destroy(){
  }
}