package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Comment;
import service.UserService;

@WebServlet(urlPatterns = { "/commentDelete" })
public class CommentDeleteServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    	Comment commentDelete = new Comment();
        commentDelete.setId(Integer.parseInt(request.getParameter("comment_id")));

        new UserService().deleteComment(commentDelete);

        response.sendRedirect("./");

    }
}
