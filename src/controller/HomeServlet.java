package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.UserComment;
import beans.UserMessage;
import service.CommentService;
import service.MessageService;

@WebServlet(urlPatterns = { "/index.jsp" })
public class HomeServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {
    	 HttpSession session = request.getSession();

		 Date date = new Date();
		 String fromday = request.getParameter("fromday");
		 String untilday = request.getParameter("untilday");
		 String category = request.getParameter("category");

		 request.setAttribute("fromday", fromday);
		 request.setAttribute("untilday", untilday);
		 request.setAttribute("category", category);



		 if(StringUtils.isEmpty(fromday) && StringUtils.isEmpty(untilday)){
			 fromday = "2018-06-01";
			 untilday = date.toString();
		 }else if(!StringUtils.isEmpty(fromday) && !StringUtils.isEmpty(untilday)){
		 }else if(!StringUtils.isEmpty(request.getParameter("fromday"))){
			 untilday = date.toString();
		 }else if(!StringUtils.isEmpty(request.getParameter("untilday"))){
			 fromday = "2018-06-01";
		 }

		 List<UserMessage> contributions = new MessageService().getMessage(fromday,untilday,category);
		 List<UserComment> comments = new CommentService().getComment();
		 session.setAttribute("contributions", contributions );
		 session.setAttribute("comments", comments);

		 if(!contributions .isEmpty()){
			 request.getRequestDispatcher("/home.jsp").forward(request, response);
		 }else{
			List<String> narrowingMessages = new ArrayList<String>();
			narrowingMessages.add("絞り込み条件に一致する投稿がありません。");
			session.setAttribute("narrowingMessages", narrowingMessages);
	        request.getRequestDispatcher("/home.jsp").forward(request, response);
		 }
    }
}