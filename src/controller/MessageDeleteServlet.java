package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Message;
import service.UserService;

@WebServlet(urlPatterns = { "/messageDelete" })
public class MessageDeleteServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {


        Message messageDelete = new Message();
        messageDelete.setId(Integer.parseInt(request.getParameter("message_id")));

        new UserService().delete(messageDelete);


        response.sendRedirect("./");


    }
}