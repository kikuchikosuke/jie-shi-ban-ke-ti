package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.User;
import service.BranchService;
import service.PositionService;
import service.UserService;

@WebServlet(urlPatterns = { "/edit" })
public class EditServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    List<User> branch = new BranchService().getBranch();
	List<User> position = new PositionService().getPosition();

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

    			//urlIDがnullか判断
    			if(request.getParameter("id")==null){
    				HttpSession session = request.getSession();
    				session.setAttribute("errorMessages", "IDが不正です。");
    				response.sendRedirect("management");
    				return;
    			}

    			//urlIDが数字か判断
    			String idCheck = request.getParameter("id");
    			if( !idCheck.matches("[0-9]{1,10}")){
    				HttpSession session = request.getSession();
    				session.setAttribute("errorMessages", "IDが不正です。");
    				response.sendRedirect("management");
    				return;
    			}

    			int id = Integer.parseInt(request.getParameter("id"));
				User editUser = new UserService().getUser(id);
				User editName = new UserService().getUser(id);
				boolean idc = new UserService().getId(id);

				//urlのidが存在するか判断
    			if(idc == true){
    				HttpSession session = request.getSession();
    				session.setAttribute("errorMessages", "IDが不正です。");
    				response.sendRedirect("management");
    				return;
    			}else{
    				HttpSession session = request.getSession();
    				//urlが適正なときの処理
					request.setAttribute("editUser", editUser);
					session.setAttribute("editName", editName);
	    			request.setAttribute("branch", branch);
	    			request.setAttribute("position", position);
	    			request.getRequestDispatcher("edit.jsp").forward(request, response);
    			}
    }
    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

        List<String> messages = new ArrayList<String>();
        HttpSession session = request.getSession();
        User editUser = getEditUser(request);

        if (isValid(request, messages) == true){
        	if(request.getParameter("id").equals(request.getParameter("loginUserId"))){
        		if(StringUtils.isEmpty(request.getParameter("password"))){
        			new UserService().updateReserve(editUser);
        			session.setAttribute("loginUser", editUser);
        		}else{
        			new UserService().update(editUser);
        			session.setAttribute("loginUser", editUser);
        		}
        		response.sendRedirect("management");
        	}else if(!request.getParameter("id").equals(request.getParameter("loginUserId"))){
        		if(StringUtils.isEmpty(request.getParameter("password"))){
        			new UserService().updateReserve(editUser);
        		}else{
        			new UserService().update(editUser);
        		}
        		response.sendRedirect("management");
        	}
        }else{
        	session.setAttribute("errorMessages", messages);
            request.setAttribute("editUser", editUser);
            request.setAttribute("branch", branch);
			request.setAttribute("position", position);
            request.getRequestDispatcher("edit.jsp").forward(request, response);
        }
    }

    private User getEditUser(HttpServletRequest request)
            throws IOException, ServletException {

        User editUser = new User();
        editUser.setId(Integer.parseInt(request.getParameter("id")));
        editUser.setLogin_id(request.getParameter("login_id"));
        editUser.setPassword(request.getParameter("password"));
        editUser.setName(request.getParameter("name"));
        editUser.setBranch_id(Integer.parseInt(request.getParameter("branch_id")));
        editUser.setPosition_id(Integer.parseInt(request.getParameter("position_id")));
        return editUser;
    }


    private boolean isValid(HttpServletRequest request, List<String> messages) {

        String login_id = request.getParameter("login_id");
        String password = request.getParameter("password");
        String passwordCheck = request.getParameter("passwordCheck");
        String name = request.getParameter("name");
        String branch_id =request.getParameter("branch_id");
        String position_id = request.getParameter("position_id");
        int id = Integer.parseInt(request.getParameter("id"));
        boolean idCheck = new UserService().getIdCheck(id,login_id);

        if(idCheck == false){
        	messages.add("ログインIDがすでに使用されています。");
        }
        if (StringUtils.isBlank(login_id) == true) {
            messages.add("ログインIDを入力してください。");
        }
        if(StringUtils.isBlank(login_id) == false &&!login_id.matches("[0-9a-zA-Z]{6,20}")){
        	messages.add("ログインIDは半角英数字で6から20文字以下です。");
        }
        if(StringUtils.isEmpty(password) == false &&!password.matches("[!-~]{6,20}")){
        	messages.add("パスワードは記号を含むすべての半角文字で6から20文字以下です。");
        }
        if(!password.equals(passwordCheck)){
        	messages.add("パスワードと確認用パスワードが一致しません。");
        }
        if (StringUtils.isBlank(name) == true) {
            messages.add("名前を入力してください。");
        }
        if(10 < name.length()){
        	messages.add("名前は10文字以下です。");
        }
        if(position_id.matches("[3-4]") && !branch_id.matches("[1]")){
        	messages.add("支店名と役職の組み合わせが不正です。");
        }
        if(position_id.matches("[1-2]") && branch_id.matches("[1]")){
        	messages.add("支店名と役職の組み合わせが不正です。");
        }
        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

}