package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.User;
import service.BranchService;
import service.PositionService;
import service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    List<User> branch = new BranchService().getBranch();
	List<User> position = new PositionService().getPosition();

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {


		request.setAttribute("branch", branch);
		request.setAttribute("position", position);

        request.getRequestDispatcher("signup.jsp").forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        List<String> messages = new ArrayList<String>();

        HttpSession session = request.getSession();

        if (isValid(request, messages) == true) {

            User user = new User();
            user.setLogin_id(request.getParameter("login_id"));
            user.setPassword(request.getParameter("password"));
            user.setName(request.getParameter("name"));
            user.setBranch_id(Integer.parseInt(request.getParameter("branch_id")));
            user.setPosition_id(Integer.parseInt(request.getParameter("position_id")));

            new UserService().register(user);

            response.sendRedirect("management");
        } else {
            session.setAttribute("errorMessages", messages);

            request.setAttribute("branch_id", request.getParameter("branch_id"));
    		request.setAttribute("position_id", request.getParameter("position_id"));

    		request.setAttribute("login_id", request.getParameter("login_id"));
    		request.setAttribute("name", request.getParameter("name"));
            request.setAttribute("branch", branch);
			request.setAttribute("position", position);
            request.getRequestDispatcher("signup.jsp").forward(request, response);
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {
        String login_id = request.getParameter("login_id");
        String password = request.getParameter("password");
        String passwordCheck = request.getParameter("passwordCheck");
        String name = request.getParameter("name");
        String branch_id =request.getParameter("branch_id");
        String position_id = request.getParameter("position_id");
        boolean idCheck = new UserService().getLogin_id(login_id);

        //ログインIDが利用されていないか確認
        if(idCheck == false){
        	messages.add("ログインIDがすでに使用されています。");
        }
        if (StringUtils.isBlank(login_id) == true) {
            messages.add("ログインIDを入力してください。");
        }
        if(StringUtils.isEmpty(login_id) == false &&!login_id.matches("[0-9a-zA-Z]{6,20}")){
        	messages.add("ログインIDは半角英数字で6から20文字以下です。");
        }
        if (StringUtils.isEmpty(password) == true) {
            messages.add("パスワードを入力してください。");
        }
        if(StringUtils.isEmpty(password) == false &&!password.matches("[!-~]{6,20}")){
        	messages.add("パスワードは記号を含むすべての半角文字で6から20文字以下です。");
        }
        if(!password.equals(passwordCheck)){
        	messages.add("パスワードと確認用パスワードが一致しません。");
        }
        if (StringUtils.isBlank(name) == true) {
            messages.add("名前を入力してください。");
        }
        if(10 < name.length()){
        	messages.add("名前は10文字以下です。");
        }
        if(branch_id.matches("[1]") && !position_id.matches("[3-4]")){
        	messages.add("支店名と役職の組み合わせが不正です。");
        }
        if(!branch_id.matches("[1]") && !position_id.matches("[1-2]")){
        	messages.add("支店名と役職の組み合わせが不正です。");
        }
        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

}