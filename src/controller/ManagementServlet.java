
package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.User;
import service.ManagementService;
import service.UserService;


@WebServlet(urlPatterns = { "/management" })
public class ManagementServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    	List<User> user = new ManagementService().getUser();
    	request.setAttribute("users", user);
        request.getRequestDispatcher("/management.jsp").forward(request, response);
    }
    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

    	User is_stoppeds = new User();
        is_stoppeds.setId(Integer.parseInt(request.getParameter("id")));
        is_stoppeds.setIs_stopped(Integer.parseInt(request.getParameter("is_stopped")));

        new UserService().is_stopped(is_stoppeds);

        response.sendRedirect("management");
        }
}
