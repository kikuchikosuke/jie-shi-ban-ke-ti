package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.User;
import dao.UserManagementDao;

public class ManagementService {
    private static final int LIMIT_NUM = 1000;

    public List<User> getUser() {

        Connection connection = null;
        try {
            connection = getConnection();

            UserManagementDao userManagementDao = new UserManagementDao();
            List<User> ret = userManagementDao.getUserManagement(connection , LIMIT_NUM);

            commit(connection);

            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

}