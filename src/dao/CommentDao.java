package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import beans.Comment;
import exception.NoRowsUpdatedRuntimeException;
import exception.SQLRuntimeException;

public class CommentDao {

    public void insert(Connection connection, Comment comment) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO comments ( ");
            sql.append("message_id");
            sql.append(", comment");
            sql.append(", created_date");
            sql.append(", user_id");
            sql.append(") VALUES (");
            sql.append(" ?"); // message_id
            sql.append(", ?"); // comment
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", ?"); // user_id
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setInt(1, comment.getMessage_id());
            ps.setString(2, comment.getComment());
            ps.setInt(3, comment.getUser_id());

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
    public void deleteComment(Connection connection, Comment comment_id) {

        PreparedStatement ps = null;
        try {
            String sql = "DELETE FROM comments WHERE id = ? ";

            ps = connection.prepareStatement(sql.toString());

            ps.setInt(1,comment_id.getId());

            int count = ps.executeUpdate();
            if (count == 0) {
                throw new NoRowsUpdatedRuntimeException();
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
  }

}