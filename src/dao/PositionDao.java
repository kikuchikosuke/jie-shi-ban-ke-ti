package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.User;
import exception.SQLRuntimeException;

public class PositionDao {

	public List<User> getPosition(Connection connection) {

	    PreparedStatement ps = null;
	    try {
	        String sql = "SELECT * FROM positions";

	        ps = connection.prepareStatement(sql);

	        ResultSet rs = ps.executeQuery();
            List<User> ret = toUserList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
	}
	private List<User> toUserList(ResultSet rs) throws SQLException {

        List<User> ret = new ArrayList<User>();
        try {
            while (rs.next()) {
                int id = rs.getInt("id");
                String position_name = rs.getString("position_name");

                User position = new User();
                position.setId(id);
                position.setPosition_name(position_name);

                ret.add(position);
            }
            return ret;
        } finally {
            close(rs);
        }
    }
}
