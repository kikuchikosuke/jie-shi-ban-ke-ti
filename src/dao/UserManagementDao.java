package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.User;
import exception.SQLRuntimeException;

public class UserManagementDao {

    public List<User> getUserManagement(Connection connection, int num) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("users.id as id, ");
            sql.append("users.login_id as login_id, ");
            sql.append("users.name as name, ");
            sql.append("users.is_stopped as is_stopped, ");
            sql.append("branchs.branch_name as branch_name, ");
            sql.append("positions.position_name as position_name, ");
            sql.append("users.created_date as created_date ");
            sql.append("FROM users ");
            sql.append("INNER JOIN branchs ");
            sql.append("ON users.branch_id = branchs.id ");
            sql.append("INNER JOIN positions ");
            sql.append("ON users.position_id = positions.id ");
            sql.append("ORDER BY id ASC limit " +num);

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<User> ret = toUserList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<User> toUserList(ResultSet rs)
            throws SQLException {

        List<User> ret = new ArrayList<User>();
        try {
            while (rs.next()) {
                int id = rs.getInt("id");
                String login_id = rs.getString("login_id");
                String name = rs.getString("name");
                String branch_name = rs.getString("branch_name");
                String position_name = rs.getString("position_name");
                int is_stopped = rs.getInt("is_stopped");
                Timestamp createdDate = rs.getTimestamp("created_date");

                User user = new User();
                user.setId(id);
                user.setLogin_id(login_id);
                user.setName(name);
                user.setBranch_name(branch_name);
                user.setPosition_name(position_name);
                user.setIs_stopped(is_stopped);
                user.setCreatedDate(createdDate);

                ret.add(user);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

}