package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.UserMessage;
import exception.SQLRuntimeException;

public class UserMessageDao {

    public List<UserMessage> getUserMessages(Connection connection,String fromday,String untilday,String category, int num ) {

    	 PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();

            sql.append("SELECT ");
            sql.append("messages.id as id, ");
            sql.append("messages.subject as subject, ");
            sql.append("messages.category as category, ");
            sql.append("messages.text as text, ");
            sql.append("messages.user_id as user_id,");
            sql.append("users.name as name,");
            sql.append("messages.created_date as created_date ");
            sql.append("FROM messages ");
            sql.append("INNER JOIN users ");
            sql.append("ON messages.user_id = users.id ");
            sql.append("WHERE category LIKE ? " );
            sql.append("AND messages.created_date >=  ?  AND messages.created_date <  ? ORDER BY created_date DESC limit " + num);

            if(StringUtils.isEmpty(category)){
            	ps = connection.prepareStatement(sql.toString());
             	ps.setString(1, "%"+""+"%");
             	ps.setString(2, fromday+" 00:00:00");
             	ps.setString(3, untilday+" 23:59:59");
            }else{
            	 ps = connection.prepareStatement(sql.toString());
                 	ps.setString(1, "%"+category+"%");
                 	ps.setString(2, fromday+" 00:00:00");
                 	ps.setString(3, untilday+" 23:59:59");
            }

            ResultSet rs = ps.executeQuery();
            List<UserMessage> ret = toUserMessageList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }

    }


    private List<UserMessage> toUserMessageList(ResultSet rs)
            throws SQLException {

        List<UserMessage> ret = new ArrayList<UserMessage>();
        try {
            while (rs.next()) {
                String subject = rs.getString("subject");
                String category = rs.getString("category");
                String text = rs.getString("text");
                String name = rs.getString("name");
                int id = rs.getInt("id");
                int user_id = rs.getInt("user_id");
                Timestamp createdDate = rs.getTimestamp("created_date");

                UserMessage message = new UserMessage();
                message.setSubject(subject);
                message.setCategory(category);
                message.setName(name);
                message.setText(text);
                message.setId(id);
                message.setUser_id(user_id);
                message.setCreated_date(createdDate);

                ret.add(message);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

}