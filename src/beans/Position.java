package beans;

import java.io.Serializable;
public class Position implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    private int position_name;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getPosition_name() {
		return position_name;
	}
	public void setPosition_name(int position_name) {
		this.position_name = position_name;
	}
}