<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>ユーザー新規登録</title>
    <link href="./CSS/signup.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="main-contents">
        <a href="./">ホーム画面</a>
        <a href="management">>ユーザー管理画面</a>
        <a>>新規ユーザー登録画面</a>
        <br/>

        <div class="name"> <h3>ようこそ<c:out value="${loginUser.name}" />さん</h3></div>


            <div class="name" style="text-align : center"> <h3>新規ユーザー登録</h3></div>
             <c:if test="${ not empty errorMessages }">
                <div class="errorMessages" align="center">
                        <c:forEach items="${errorMessages}" var="message">
                            <font color="red"><c:out value="${message}" /><br/></font>
                        </c:forEach>
                </div>
                <c:remove var="errorMessages" scope="session" />
            </c:if>
            <form action="signup" method="post"><br />
            <table border="1" align="center">
            <tr><th align="center"><label for="login_id">ログインID</label></th><td><input name="login_id" value="${login_id}" id="login_id" />(半角英数字6～20)</td></tr>
            <tr><th align="center"><label for="password">パスワード</label></th><td><input name="password" type="password" id="password" />(記号を含むすべての半角文字6～20)</td></tr>
            <tr><th align="center"><label for="passwordCheck">パスワード(確認用)</label></th><td><input name="passwordCheck" type="password" id="passwordCheck" /><br /></td></tr>
            <tr><th align="center"><label for="name">名前</label></th><td><input name="name" value="${name}" id="name" />(10文字以内)</td></tr>
            <tr><th align="center"><label for="branch_id">支店名</label></th>
              	<td><select name="branch_id" style="width:175px">
              		<c:forEach items="${branch}" var="branch">
    					<option id="branch" value="${branch.id}" <c:if test="${branch_id eq branch.id}">selected</c:if> >${branch.branch_name}</option>
   				 	</c:forEach>
				</select></td></tr>

            <tr><th align="center"><label for="position_id">役職</label></th>
               	<td><select name="position_id" style="width:175px">
  					<c:forEach items="${position}" var="position">
    					<option id="position" value="${position.id}" <c:if test="${position_id eq position.id}">selected</c:if> >${position.position_name}</option>
   				 	</c:forEach>
				</select></td></tr>
            </table>
            <br/>
            <div align="center">
            <input type="submit" value="新規登録" />
            </div>
            </form>
            <br/>
            <div class="copyright" style="text-align : center">Copyright(c)kikuchi.kosuke</div>
        </div>
    </body>
</html>