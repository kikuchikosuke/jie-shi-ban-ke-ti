<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>新規投稿</title>
    <link href="./CSS/signup.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="main-contents">
         <a href="./">ホーム画面</a>
         <a>>新規投稿画面</a>
        <br/>

        <div class="name"> <h3>ようこそ<c:out value="${loginUser.name}" />さん</h3></div>

           <div class="form-area">
        <form action="newMessage" method="post">

        <div class="name" style="text-align : center"> <h3>新規投稿画面</h3></div>
        <c:if test="${ not empty errorMessages }">
                <div class="errorMessages" align="center">
                        <c:forEach items="${errorMessages}" var="message">
                            <font color="red"><c:out value="${message}" /><br/></font>
                        </c:forEach>
                </div>
                <c:remove var="errorMessages" scope="session" />
            </c:if>
        <table border="1" align="center">
        <tr>
        <td><label for="subject">件名</label></td><td><input name="subject" value="${subject}" id="subject" maxlength='30' style="width:300px"/>(30文字以下)<br /></td>
        </tr>
        <tr>
        <td><label for="category">カテゴリー</label></td><td><input name="category" value="${category}" id="category" maxlength='10' style="width:300px"/>(10文字以下)<br /></td>
        </tr>
        <tr><td class="td"></td></tr>
        <tr><td colspan=2 class="td">本文(1000文字以下)</td></tr>
        <tr><td colspan=2><textarea name="text" cols="100" rows="20" id="newMessage">${text}</textarea><br /></td></tr>
        </table>
        <br/>
        <div align="center">
            <input type="submit" value="投　稿　!">
        </div>
        </form>
</div>
<br/>
            <div class="copyright" style="text-align : center">Copyright(c)kikuchi.kosuke</div>
        </div>
    </body>
</html>