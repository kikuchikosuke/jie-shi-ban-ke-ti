<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>ユーザー管理</title>
        <link href="CSS/management.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="main-contents">
        <a href="./">ホーム画面</a>
        <a>>ユーザー管理画面</a>

        <br/>

        <div class="name"> <h3>ようこそ<c:out value="${loginUser.name}" />さん</h3></div>

            <div class="name" style="text-align : center"> <h3>ユーザー管理</h3></div>
    </div>
    <c:if test="${ not empty errorMessages }">
                <div class="errorMessages" align="center">
                        <c:forEach items="${errorMessages}" var="comments">
                            <font color="red"><c:out value="${comments}" /><br/></font>
                        </c:forEach>
                </div>
                <c:remove var="errorMessages" scope="session" />
            </c:if>
            <div align="center">
 			<a href="signup" >新規ユーザー登録</a>
 			</div>
<table border="1" align="center">
     <tr>
     	<th>ID</th>
     	<th>LOGIN-ID</th>
     	<th>名前</th>
     	<th>支店名</th>
     	<th>役職</th>
     	<th>ユーザー管理</th>
     	<th>ユーザー編集</th>
     </tr>
     <c:forEach items="${users}" var="users">
     	<tr>
			<td style="text-align : center"><c:out  value="${users.id}" /></td>
            <td style="text-align : center"><c:out  value="${users.login_id}" /></td>
            <td style="text-align : center"><c:out  value="${users.name}" /></td>
            <td style="text-align : center"><c:out  value="${users.branch_name}" /></td>
            <td style="text-align : center"><c:out  value="${users.position_name}" /></td>
            <td style="text-align : center">
            <c:if test="${users.id eq loginUser.id}">
            <c:out  value="ログイン中"/>
            </c:if>
            <c:if test="${users.id ne loginUser.id}">
            <form action="management" method="post">
            <input type ="hidden" name ="id" value ="${users.id}" >
            	<c:if test="${users.is_stopped == 0}">
            	<input style="text-align : center" name="btn" type="submit" value="停止" onClick="return check('停止','${users.name}')">
            	<input type="hidden" name="is_stopped" value="1">
    			</c:if>
    			<c:if test="${users.is_stopped == 1}">
            	<input style="text-align : center" name="btn" type="submit" value="復活" onClick="return check('復活','${users.name}')">
            	<input type="hidden" name="is_stopped" value="0">
    			</c:if>
        		</form>
        	</c:if>

				<!-- 確認ダイアログ -->
        		<script type="text/javascript">
				function check(message,userName){
					if(window.confirm( userName+'さんのアカウントを'+message+'しますか？')){ // 確認ダイアログを表示
						return true; // 「OK」時は送信を実行
					}
				else{ // 「キャンセル」時の処理
					return false; // 送信を中止
					}
				}
				</script>

        		</td>
            <td style="text-align : center"><a href="edit?id=${users.id}" >編集する</a></td>
         </tr>
      </c:forEach>
</table>
			<br/>
            <div class="copyright" style="text-align : center"> Copyright(c)kikuchi.kosuke</div>

    </body>
</html>
