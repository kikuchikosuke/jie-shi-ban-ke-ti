<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>ホーム</title>
        <link href="./CSS/home.css" rel="stylesheet" type="text/css">
    </head>
    <body>

        <div class="main-contents">
        <div id="header-fixed">
        	<div id="header-bk">
        		<div id="header">
        <div>
        <c:if test="${loginUser.position_id == 3 }">
        <a class="managementbutton" href="management">ユーザー管理画面</a>
        </c:if>
        <a class="logoutbutton" href="logout">ログアウト</a>
        </div>
        <br/>
        <h3>ようこそ<c:out value="${loginUser.name}" />さん</h3>

	<h3 align="center">投稿一覧</h3>
	<form action="./">
	<div>
	<a href="newMessage" class="square_btn">新規投稿する</a>
	<a href="./" class="categoryReset">絞込み条件初期化</a>
	<a class="category">日付検索<input type="date" id="fromday" name="fromday" value="${fromday}">～
	<input type="date" id="untilday" name="untilday" value="${untilday}">
	カテゴリー検索<input name="category" id="category" value="${category}">
	<input type="submit" value="絞込み"></a>

	</div>

	</form>


</div>
</div>
</div>
<div id="body">
<!-- エラーメッセージ-->
<c:if test="${ not empty errorMessages }">
                <div class="errorMessages" align="center">
                        <c:forEach items="${errorMessages}" var="comments">
                            <font color="red"><c:out value="${comments}" /><br/></font>
                        </c:forEach>
                </div>
                <c:remove var="errorMessages" scope="session" />
            </c:if>
<!-- 絞込みエラーメッセージ -->
            <c:if test="${ not empty narrowingMessages }">
                <div class="narrowingMessages" align="center">
                        <c:forEach items="${narrowingMessages}" var="narrowing">
                     		<c:out value="${narrowing}" />
                        </c:forEach>
                </div>
                <c:remove var="narrowingMessages" scope="session" />
            </c:if>

<div  class="messages">
    <c:forEach items="${contributions}" var="message">


				<div class="comments">
				<div class="commentslist">
    			<c:forEach items="${comments}" var="comment">
    			<c:if test="${comment.message_id == message.id }">
            <div class="comment">

                <table border="1" class="comment">
                	<tr>
                <td align="justify" valign="top" class="td" width="500"><c:forEach var="str" items="${fn:split(comment.comment,'
						')}" ><c:out value="${str}" /><br></c:forEach></td>
                	</tr>
                	<tr><td class="td"><div><a class="commentname"><c:out value="${comment.name}" />
                	:<fmt:formatDate value="${comment.created_date}" pattern="yyyy/MM/dd HH:mm:ss" /></a>
                	 <c:if test="${loginUser.id == comment.user_id }">
                	<form action="commentDelete" method="post">
            			<input name="btn" id="btn" type="submit" value="コメント削除" onClick="return check('コメント','${comment.name}')">
            			<input type ="hidden" name ="comment_id" value ="${comment.id}" >
        			</form>
        			</c:if></div></td></tr>
                </table>



        		<!-- 確認ダイアログ -->
        		<script type="text/javascript">
				function check(message,userName){
					if(window.confirm( userName+'さんの'+message+'を削除しますか？')){ // 確認ダイアログを表示
						return true; // 「OK」時は送信を実行
					}
				else{ // 「キャンセル」時の処理
					return false; // 送信を中止
					}
				}
				</script>
            </div>

            </c:if>
    			</c:forEach>
    			</div>


				<br/>
				<div>
						<form action="newComment" method="post">
            			コメント<br />
		            	<textarea name="comment" id="comment"><c:if test="${message.id == message_id }">${errorComment}</c:if></textarea>
						<br />
		            	<input type="submit" value="コメント" align="right">
		            	<input type ="hidden" name ="message_id" value ="${message.id}" >
		        		</form>
					</div>
					</div>
                	<table border="5" class="text">
                	<tr><td class="td" width="10"><label>件名:</label><c:out value="${message.subject}" /></td></tr>
                	<tr><td colspan=2 align="justify" valign="top" height="200">
                		<div class="display">
						<c:forEach var="str" items="${fn:split(message.text,'
						')}" ><c:out value="${str}" /><br></c:forEach></div></td></tr>
                	<tr><td class="td"><div>カテゴリー:<c:out value="${message.category}"/></div>
                	<div>投稿者:<c:out value="${message.name}"/></div></td></tr>
                	<tr><td class="td" width="800">
                	<fmt:formatDate value="${message.created_date}" pattern="yyyy/MM/dd HH:mm:ss" /></td>
                	<td class="td"><c:if test="${loginUser.id == message.user_id }">
                	<form action="messageDelete" method="post">
            		<input  align="right" name="btn" type="submit" value="投稿削除" onClick="return check('投稿','${message.name}')">
            		<input type ="hidden" name ="message_id" value ="${message.id}" >
        			</form>
        			</c:if></td></tr>
                	</table>

<br/>
<hr>
<br/>
	</c:forEach>
	<c:remove var="errorComment" scope="session" />
	<c:remove var="message_id" scope="session" />
</div>

            <div class="copyright" style="text-align : center"> Copyright(c)kikuchi.kosuke</div>
        </div>
        </div>
    </body>
</html>